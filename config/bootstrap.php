<?php
use Cake\Core\Configure;
use Cake\Network\Request;
use Cake\Core\Plugin;
use Cake\Routing\Router;

Router::extensions(['csv','xlsx','ods']);

Request::addDetector('csv', function($request) {
    return ($request->param('_ext') === 'csv' 
        || $request->query('export') === 'csv'
        //|| $request->env('HTTP_ACCEPT') === 'text/csv'
    );
});

Request::addDetector('xlsx', function($request) {
    return ($request->param('_ext') === 'xlsx' 
        || $request->query('export') === 'xlsx');
});

Request::addDetector('xlsx', function($request) {
    return ($request->param('_ext') === 'ods' 
        || $request->query('export') === 'ods');
});
