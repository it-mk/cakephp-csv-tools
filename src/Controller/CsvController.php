<?php

namespace MK\CsvTools\Controller;

use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class CsvController extends Controller
{

    /**
     * Creates plain text response, rendering specified view, or 
     * default view associated with action.
     * @param array $options - Available options:
     *      data: array|string - If array, data is set as view vars. If string, 
     *          it treats it as already rendered text content. Ignores view and performs no further rendering.
     *      view: Name of template to render,
     *      layout: Name of layout to use. False for no layout. Null for default
     * @return \Cake\Network\Response 
     */
    protected function csvResponse($options=[],$errors=null)
    {
        $options += [
            'data' => null,
            'view' => null,
            'layout' => false
        ];

        $this->autoRender = false;
        
        if(!is_string($options['data'])) {
            $builder = $this->viewBuilder();
            $builder->layout($options['layout']);
            if (!$builder->templatePath()) {
                $builder->templatePath($this->_viewPath());
            }
                           
            $builder->className($this->viewClass);

            if ($builder->template() === null &&
                isset($this->request->params['action'])
            ) {
                $builder->template($this->request->params['action']);
            }
            if(is_array($options['data'])) {
                $this->set($options['data']);
            }
            if(!empty($errors)) {
                $this->set(compact('errors'));
            }

            $this->View = $this->createView();
            $content = $this->View->render($options['view'], $options['layout']);
        } else {
            $content = $options['data'];
        }
     
        $this->response->type('text/csv');
        $this->response->body($content);
        return $this->response;
    }
    
}
